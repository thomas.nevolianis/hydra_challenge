Steps:

1) Solute optimization at b3lypd3/tzvp
2) Solute-water cluster searching of conformers at GFN2-xTB (default convergence settings) using CREST
3) Selection of the clusters based on boltzmann distribution at GFN2-xTB energy
4) Solute-water cluster optimization at b3lypd3/tzvp
5) Solute-water cluster single point energy at b2plypd3/aug-cc-pvtz
6) Checks for duplicates based on moments of inertia, RMSD, energies(b2plypd3/aug-cc-pvtz)
7) Summary.txt with the energy of the conformers

Solutes:
Cyclooctanone CON 502-49-8
1,3-Dimethyl-2-imidazolidinone DMI 80-73-9
Formaldehyde FAH 50-00-0
Methyl lactate MLA 547-64-8
1-Phenylcyclohexane-cis-1,2-diol PCD 125132-75-4
Pyridine PYR 110-86-1
Tetrahydrofuran THF 109-99-9
Tetrahydrothiophene THT 110-01-0
2,2,2-Trifluoroacetophenone TPH 434-45-7
2,2,2-Trifluoroethan-1-ol TFE 75-89-8


